# GUIA MUESTREO #

Este repositorio está pensado para administrar la información de muestreo en "El tomillo" como datos de collección y procesamiento de las muestras, a través de la integración mediante códigos QR.

##Descarga de app y mapa para trabajar con GPS (sin datos) en su **CELULAR**

Para marcar las coordenadas en el mapa se usara [QFIELD](https://play.google.com/store/apps/details?id=ch.opengis.qfield&hl=es_AR) y descargar los 5 archivos en carpeta del [drive](https://drive.google.com/drive/folders/1zWTHDHtPvzFOKDEhpz2U7WM00RLzk7a8?usp=sharing)

##MAPA EL TOMILLO
https://www.google.com/maps/d/u/0/edit?mid=1LxrwNYYlDg6JrkEXMATEw5g8sH9GBN-5&ll=-33.40277905515109%2C-69.21925412821395&z=16
##IMG del mapa con sitios de muestreo
![map](https://drive.google.com/uc?export=view&id=13m2vLsThEZ21nibws7ySzN51FiyuH0d_)

##Esquema de muestreo
![esquema](https://drive.google.com/uc?export=view&id=1s9TNlckyqC808uZWYG0-qAe5Ltq5DrDF)

### Codificación de muestras ###

Tipo de muestra | Cuartel | Sector | Réplica | Número (fecha) 
--- | --- | --- | ---
S (suelo) | 8 u 11 | A,B o C | 1,2 o 3 | 7.3.19
B (baya) |
R (ritidomis) |
H (hojas) |

####Ejemplo:
muestra ritidomis cuartel 11 D sector C –réplica  2 día 7-3-19): **R11C2_7-7-19**

##Fotos día de muestreo
![IMG0](https://drive.google.com/uc?export=view&id=1JuVctECIS-EVAFuGB2pMq2VG8EyJQQen)
![IMG1](https://drive.google.com/uc?export=view&id=1whL5_Tq55_4xMQUdB45EE4xufGrs2kJZ)
![IMG2](https://drive.google.com/uc?export=view&id=1oia7NAGOmXRT3zYoaPG85634XMueJt4D)
![IMG3](https://drive.google.com/uc?export=view&id=1-dJgc2l7kci3cTlBaJEEf9dQuc0ECm82)
![IMG4](https://drive.google.com/uc?export=view&id=1yJ1jwCmOaQDDMTfSTXLkXZ9ab1Ygj_X9)

#Vinificaishon ;-)

![IMG5](https://drive.google.com/uc?export=view&id=1kafNJRhylKsX1NNdNroYQpjgByZ-Xoyn)

#MUESTRAS

###**[8A1_7.3.19](https://bitbucket.org/metagenomics/data_collection_info/src/master/8A1_7.3.19.md)**
#EEA_INTA_Mendoza
![logo INTA](https://drive.google.com/uc?export=view&id=1aCpxcAYOWxNxzd9GLXJO27qG4pfYzF52)

UUIDs
538f97cf-e25d-4f31-86e5-216618845117
QRcode
![alt text](https://qrcode.tec-it.com/API/QRCode?data=https://bitbucket.org/metagenomics/data_collection_info/src/77dbda7ee146b866cfa5d511f690f39809445aa5/8A1_7.3.19.md&backcolor=%23ffffff)

Tipo de muestra | Código | Coordenadas GPS lat | Coordenadas GPS long | Observaciones
--- | --- | --- | --- | ---
Suelo | S8A1_7.3.19 | --- | --- | ---
Baya | B8A1_7.3.19 | --- | --- | ---
Ritidomis | R8A1_7.3.19 | --- | --- | ---
Hojas | H8A1_7.3.19 | --- | --- | ---


###**[8A2_7.3.19](https://bitbucket.org/metagenomics/data_collection_info/src/master/8A2_7.3.19.md)**
#EEA_INTA_Mendoza
![logo INTA](https://drive.google.com/uc?export=view&id=1aCpxcAYOWxNxzd9GLXJO27qG4pfYzF52)

UUIDs
98cef78a-6e67-4aee-91e2-576cd6eb3026
QRcode
![alt text](https://qrcode.tec-it.com/API/QRCode?data=https://bitbucket.org/metagenomics/data_collection_info/src/master/8A2_7.3.19.md&backcolor=%23ffffff)

Tipo de muestra | Código | Coordenadas GPS lat | Coordenadas GPS long | Observaciones
--- | --- | --- | --- | ---
Suelo | S8A2_7.3.19 | --- | --- | ---
Baya | B8A2_7.3.19 | --- | --- | ---
Ritidomis | R8A2_7.3.19 | --- | --- | ---
Hojas | H8A2_7.3.19 | --- | --- | ---

###**[8A3_7.3.19](https://bitbucket.org/metagenomics/data_collection_info/src/master/8A3_7.3.19.md)**
#EEA_INTA_Mendoza
![logo INTA](https://drive.google.com/uc?export=view&id=1aCpxcAYOWxNxzd9GLXJO27qG4pfYzF52)

UUIDs
2d8d736d-229f-40f2-9e6f-33ca59258b5c
QRcode
![alt text](https://qrcode.tec-it.com/API/QRCode?data=https://bitbucket.org/metagenomics/data_collection_info/src/master/8A3_7.3.19.md&backcolor=%23ffffff)

Tipo de muestra | Código | Coordenadas GPS lat | Coordenadas GPS long | Observaciones
--- | --- | --- | --- | ---
Suelo | S8A3_7.3.19 | --- | --- | ---
Baya | B8A3_7.3.19 | --- | --- | ---
Ritidomis | R8A3_7.3.19 | --- | --- | ---
Hojas | H8A3_7.3.19 | --- | --- | ---


###**[8B1_7.3.19](https://bitbucket.org/metagenomics/data_collection_info/src/master/8B1_7.3.19.md)**
#EEA_INTA_Mendoza
![logo INTA](https://drive.google.com/uc?export=view&id=1aCpxcAYOWxNxzd9GLXJO27qG4pfYzF52)

UUIDs
12dfdf7d-6043-45dd-b784-20d4fa6e5838
QRcode
![alt text](https://qrcode.tec-it.com/API/QRCode?data=https://bitbucket.org/metagenomics/data_collection_info/src/master/8B1_7.3.19.md&backcolor=%23ffffff)

Tipo de muestra | Código | Coordenadas GPS lat | Coordenadas GPS long | Observaciones
--- | --- | --- | --- | ---
Suelo | S8B1_7.3.19 | --- | --- | ---
Baya | B8B1_7.3.19 | --- | --- | ---
Ritidomis | R8B1_7.3.19 | --- | --- | ---
Hojas | H8B1_7.3.19 | --- | --- | ---

###**[8B2_7.3.19](https://bitbucket.org/metagenomics/data_collection_info/src/master/8B2_7.3.19.md)**
#EEA_INTA_Mendoza
![logo INTA](https://drive.google.com/uc?export=view&id=1aCpxcAYOWxNxzd9GLXJO27qG4pfYzF52)

UUIDs
7a6a9a7c-8f8a-4258-acf6-1b03d57f0bca
QRcode
![alt text](https://qrcode.tec-it.com/API/QRCode?data=https://bitbucket.org/metagenomics/data_collection_info/src/master/8B2_7.3.19.md&backcolor=%23ffffff)

Tipo de muestra | Código | Coordenadas GPS lat | Coordenadas GPS long | Observaciones
--- | --- | --- | --- | ---
Suelo | S8B2_7.3.19 | --- | --- | ---
Baya | B8B2_7.3.19 | --- | --- | ---
Ritidomis | R8B2_7.3.19 | --- | --- | ---
Hojas | H8B2_7.3.19 | --- | --- | ---

###**[8B3_7.3.19](https://bitbucket.org/metagenomics/data_collection_info/src/master/8B3_7.3.19.md)**
#EEA_INTA_Mendoza
![logo INTA](https://drive.google.com/uc?export=view&id=1aCpxcAYOWxNxzd9GLXJO27qG4pfYzF52)

UUIDs
cf2f8cf7-c587-47fa-ab6b-7c356a3a687d
QRcode
![alt text](https://qrcode.tec-it.com/API/QRCode?data=https://bitbucket.org/metagenomics/data_collection_info/src/master/8B3_7.3.19.md&backcolor=%23ffffff)

Tipo de muestra | Código | Coordenadas GPS lat | Coordenadas GPS long | Observaciones
--- | --- | --- | --- | ---
Suelo | S8B3_7.3.19 | --- | --- | ---
Baya | B8B3_7.3.19 | --- | --- | ---
Ritidomis | R8B3_7.3.19 | --- | --- | ---
Hojas | H8B3_7.3.19 | --- | --- | ---

###**[8C1_7.3.19](https://bitbucket.org/metagenomics/data_collection_info/src/master/8C1_7.3.19.md)**
#EEA_INTA_Mendoza
![logo INTA](https://drive.google.com/uc?export=view&id=1aCpxcAYOWxNxzd9GLXJO27qG4pfYzF52)

UUIDs
11087c8f-94d0-4e76-ab7b-424ba2e0f8b9
QRcode
![alt text](https://qrcode.tec-it.com/API/QRCode?data=https://bitbucket.org/metagenomics/data_collection_info/src/master/8C1_7.3.19.md&backcolor=%23ffffff)

Tipo de muestra | Código | Coordenadas GPS lat | Coordenadas GPS long | Observaciones
--- | --- | --- | --- | ---
Suelo | S8C1_7.3.19 | --- | --- | ---
Baya | B8C1_7.3.19 | --- | --- | ---
Ritidomis | R8C1_7.3.19 | --- | --- | ---
Hojas | H8C1_7.3.19 | --- | --- | ---

###**[8C2_7.3.19](https://bitbucket.org/metagenomics/data_collection_info/src/master/8C2_7.3.19.md)**
#EEA_INTA_Mendoza
![logo INTA](https://drive.google.com/uc?export=view&id=1aCpxcAYOWxNxzd9GLXJO27qG4pfYzF52)

UUIDs
27b7bf92-642f-4727-8e17-28cbf2e312ed
QRcode
![alt text](https://qrcode.tec-it.com/API/QRCode?data=https://bitbucket.org/metagenomics/data_collection_info/src/master/8C2_7.3.19.md&backcolor=%23ffffff)

Tipo de muestra | Código | Coordenadas GPS lat | Coordenadas GPS long | Observaciones
--- | --- | --- | --- | ---
Suelo | S8C2_7.3.19 | --- | --- | ---
Baya | B8C2_7.3.19 | --- | --- | ---
Ritidomis | R8C2_7.3.19 | --- | --- | ---
Hojas | H8C2_7.3.19 | --- | --- | ---

###**[8C3_7.3.19](https://bitbucket.org/metagenomics/data_collection_info/src/master/8C3_7.3.19.md)**
#EEA_INTA_Mendoza
![logo INTA](https://drive.google.com/uc?export=view&id=1aCpxcAYOWxNxzd9GLXJO27qG4pfYzF52)

UUIDs
4e3a11bd-2b44-4df4-a0e6-e2c5afab8481
QRcode
![alt text](https://qrcode.tec-it.com/API/QRCode?data=https://bitbucket.org/metagenomics/data_collection_info/src/master/8C3_7.3.19.md&backcolor=%23ffffff)

Tipo de muestra | Código | Coordenadas GPS lat | Coordenadas GPS long | Observaciones
--- | --- | --- | --- | ---
Suelo | S8C3_7.3.19 | --- | --- | ---
Baya | B8C3_7.3.19 | --- | --- | ---
Ritidomis | R8C3_7.3.19 | --- | --- | ---
Hojas | H8C3_7.3.19 | --- | --- | ---

##suelo con roca en superficie

###**[11A1_7.3.19](https://bitbucket.org/metagenomics/data_collection_info/src/master/11A1_7.3.19.md)**
#EEA_INTA_Mendoza
![logo INTA](https://drive.google.com/uc?export=view&id=1aCpxcAYOWxNxzd9GLXJO27qG4pfYzF52)

UUIDs
ef27075a-1431-4788-bc19-9e3532b8fcef
QRcode
![alt text](https://qrcode.tec-it.com/API/QRCode?data=https://bitbucket.org/metagenomics/data_collection_info/src/master/11A1_7.3.19.md&backcolor=%23ffffff)

Tipo de muestra | Código | Coordenadas GPS lat | Coordenadas GPS long | Observaciones
--- | --- | --- | --- | ---
Suelo | S11A1_7.3.19 | --- | --- | ---
Baya | B11A1_7.3.19 | --- | --- | ---
Ritidomis | R11A1_7.3.19 | --- | --- | ---
Hojas | H11A1_7.3.19 | --- | --- | ---

###**[11A2_7.3.19](https://bitbucket.org/metagenomics/data_collection_info/src/master/11A2_7.3.19.md)**
#EEA_INTA_Mendoza
![logo INTA](https://drive.google.com/uc?export=view&id=1aCpxcAYOWxNxzd9GLXJO27qG4pfYzF52)

UUIDs
60552311-f6c0-44aa-9f29-d6e682343052
QRcode
![alt text](https://qrcode.tec-it.com/API/QRCode?data=https://bitbucket.org/metagenomics/data_collection_info/src/master/11A2_7.3.19.md&backcolor=%23ffffff)

Tipo de muestra | Código | Coordenadas GPS lat | Coordenadas GPS long | Observaciones
--- | --- | --- | --- | ---
Suelo | S11A2_7.3.19 | --- | --- | ---
Baya | B11A2_7.3.19 | --- | --- | ---
Ritidomis | R11A2_7.3.19 | --- | --- | ---
Hojas | H11A2_7.3.19 | --- | --- | ---

###**[11A3_7.3.19](https://bitbucket.org/metagenomics/data_collection_info/src/master/11A3_7.3.19.md)**
#EEA_INTA_Mendoza
![logo INTA](https://drive.google.com/uc?export=view&id=1aCpxcAYOWxNxzd9GLXJO27qG4pfYzF52)

UUIDs
f125ba1d-a1dc-4fff-abf1-a59d6d6e52c1
QRcode
![alt text](https://qrcode.tec-it.com/API/QRCode?data=https://bitbucket.org/metagenomics/data_collection_info/src/master/11A3_7.3.19.md&backcolor=%23ffffff)

Tipo de muestra | Código | Coordenadas GPS lat | Coordenadas GPS long | Observaciones
--- | --- | --- | --- | ---
Suelo | S11A3_7.3.19 | --- | --- | ---
Baya | B11A3_7.3.19 | --- | --- | ---
Ritidomis | R11A3_7.3.19 | --- | --- | ---
Hojas | H11A3_7.3.19 | --- | --- | ---

###**[11B1_7.3.19](https://bitbucket.org/metagenomics/data_collection_info/src/master/11B1_7.3.19.md)**
#EEA_INTA_Mendoza
![logo INTA](https://drive.google.com/uc?export=view&id=1aCpxcAYOWxNxzd9GLXJO27qG4pfYzF52)

UUIDs
2702d1bd-d651-4ade-b6c2-23db4f5acbf9
QRcode
![alt text](https://qrcode.tec-it.com/API/QRCode?data=https://bitbucket.org/metagenomics/data_collection_info/src/master/11B1_7.3.19.md&backcolor=%23ffffff)

Tipo de muestra | Código | Coordenadas GPS lat | Coordenadas GPS long | Observaciones
--- | --- | --- | --- | ---
Suelo | S11B1_7.3.19 | --- | --- | ---
Baya | B11B1_7.3.19 | --- | --- | ---
Ritidomis | R11B1_7.3.19 | --- | --- | ---
Hojas | H11B1_7.3.19 | --- | --- | ---

###**[11B2_7.3.19](https://bitbucket.org/metagenomics/data_collection_info/src/master/11B2_7.3.19.md)**
#EEA_INTA_Mendoza
![logo INTA](https://drive.google.com/uc?export=view&id=1aCpxcAYOWxNxzd9GLXJO27qG4pfYzF52)

UUIDs
55568c73-1e6b-47c9-bd0b-270dd9923575
QRcode
![alt text](https://qrcode.tec-it.com/API/QRCode?data=https://bitbucket.org/metagenomics/data_collection_info/src/master/11B2_7.3.19.md&backcolor=%23ffffff)

Tipo de muestra | Código | Coordenadas GPS lat | Coordenadas GPS long | Observaciones
--- | --- | --- | --- | ---
Suelo | S11B2_7.3.19 | --- | --- | ---
Baya | B11B2_7.3.19 | --- | --- | ---
Ritidomis | R11B2_7.3.19 | --- | --- | ---
Hojas | H11B2_7.3.19 | --- | --- | ---

###**[11B3_7.3.19](https://bitbucket.org/metagenomics/data_collection_info/src/master/11B3_7.3.19.md)**
#EEA_INTA_Mendoza
![logo INTA](https://drive.google.com/uc?export=view&id=1aCpxcAYOWxNxzd9GLXJO27qG4pfYzF52)

UUIDs
41ca99b6-86c4-40bd-a7e7-b83831da2edb
QRcode
![alt text](https://qrcode.tec-it.com/API/QRCode?data=https://bitbucket.org/metagenomics/data_collection_info/src/master/11B3_7.3.19.md&backcolor=%23ffffff)

Tipo de muestra | Código | Coordenadas GPS lat | Coordenadas GPS long | Observaciones
--- | --- | --- | --- | ---
Suelo | S11B3_7.3.19 | --- | --- | ---
Baya | B11B3_7.3.19 | --- | --- | ---
Ritidomis | R11B3_7.3.19 | --- | --- | ---
Hojas | H11B3_7.3.19 | --- | --- | ---

###**[11C1_7.3.19](https://bitbucket.org/metagenomics/data_collection_info/src/master/11C1_7.3.19.md)**
#EEA_INTA_Mendoza
![logo INTA](https://drive.google.com/uc?export=view&id=1aCpxcAYOWxNxzd9GLXJO27qG4pfYzF52)

UUIDs
1327c45f-606b-4972-aec6-3ab337bf55bc
QRcode
![alt text](https://qrcode.tec-it.com/API/QRCode?data=https://bitbucket.org/metagenomics/data_collection_info/src/master/11C1_7.3.19.md&backcolor=%23ffffff)

Tipo de muestra | Código | Coordenadas GPS lat | Coordenadas GPS long | Observaciones
--- | --- | --- | --- | ---
Suelo | S11C1_7.3.19 | --- | --- | ---
Baya | B11C1_7.3.19 | --- | --- | ---
Ritidomis | R11C1_7.3.19 | --- | --- | ---
Hojas | H11C1_7.3.19 | --- | --- | ---

###**[11C2_7.3.19](https://bitbucket.org/metagenomics/data_collection_info/src/master/11C2_7.3.19.md)**
#EEA_INTA_Mendoza
![logo INTA](https://drive.google.com/uc?export=view&id=1aCpxcAYOWxNxzd9GLXJO27qG4pfYzF52)

UUIDs
3d76aedf-ca38-49fb-92e7-1e083686d4af
QRcode
![alt text](https://qrcode.tec-it.com/API/QRCode?data=https://bitbucket.org/metagenomics/data_collection_info/src/master/11C2_7.3.19.md&backcolor=%23ffffff)

Tipo de muestra | Código | Coordenadas GPS lat | Coordenadas GPS long | Observaciones
--- | --- | --- | --- | ---
Suelo | S11C2_7.3.19 | --- | --- | ---
Baya | B11C2_7.3.19 | --- | --- | ---
Ritidomis | R11C2_7.3.19 | --- | --- | ---
Hojas | H11C2_7.3.19 | --- | --- | ---

###**[11C3_7.3.19](https://bitbucket.org/metagenomics/data_collection_info/src/master/11C3_7.3.19.md)**
#EEA_INTA_Mendoza
![logo INTA](https://drive.google.com/uc?export=view&id=1aCpxcAYOWxNxzd9GLXJO27qG4pfYzF52)

UUIDs
62c85a77-0b29-4299-9e3f-3c97e1c5e394
QRcode
![alt text](https://qrcode.tec-it.com/API/QRCode?data=https://bitbucket.org/metagenomics/data_collection_info/src/master/11C3_7.3.19.md&backcolor=%23ffffff)

Tipo de muestra | Código | Coordenadas GPS lat | Coordenadas GPS long | Observaciones
--- | --- | --- | --- | ---
Suelo | S11C3_7.3.19 | --- | --- | ---
Baya | B11C3_7.3.19 | --- | --- | ---
Ritidomis | R11C3_7.3.19 | --- | --- | ---
Hojas | H11C3_7.3.19 | --- | --- | ---



